package com.atlassian.rest.retrofit.generator.impl;

import com.atlassian.rest.retrofit.generator.GeneratorContext;
import com.atlassian.rest.retrofit.scanresult.impl.PayloadScanResult;
import com.atlassian.rest.retrofit.util.JavaFileUtil;
import com.sun.codemodel.JCodeModel;
import org.apache.commons.lang3.ClassUtils;
import org.apache.maven.plugin.logging.Log;
import org.jsonschema2pojo.DefaultGenerationConfig;
import org.jsonschema2pojo.GsonAnnotator;
import org.jsonschema2pojo.SchemaGenerator;
import org.jsonschema2pojo.SchemaMapper;
import org.jsonschema2pojo.SchemaStore;
import org.jsonschema2pojo.rules.RuleFactory;

import java.io.File;
import java.io.IOException;

public class PayloadClassGenerator
{
    private static PayloadClassGenerator payloadClassGenerator = new PayloadClassGenerator();

    private static SchemaMapper schemaMapper = new SchemaMapper();
    static
    {
        SchemaGenerator schemaGenerator = new SchemaGenerator();
        RuleFactory ruleFactory = new RuleFactory(new DefaultGenerationConfig(), new GsonAnnotator(), new SchemaStore());
        schemaMapper = new SchemaMapper(ruleFactory, schemaGenerator);
    }

    private PayloadClassGenerator() {}

    public static PayloadClassGenerator get()
    {
        return payloadClassGenerator;
    }

    public void generate(PayloadScanResult payloadScanResult, GeneratorContext generatorContext)
    {
        String payloadDataType = payloadScanResult.getDataType();
        File payloadJavaFile = JavaFileUtil.getJavaFile(generatorContext.getOutputFolder(), payloadDataType);

        if (!payloadJavaFile.exists() || !payloadJavaFile.isFile())
        {
            generatePayloadCode(payloadScanResult, generatorContext);
        }
        else
        {
            generatorContext.getLogger().debug("Skipped generating source code for payload class" + payloadDataType
                                            + " because it exists at " + payloadJavaFile.getAbsolutePath() + " ...");
        }
    }

    public void generatePayloadCode(PayloadScanResult payloadScanResult, GeneratorContext generatorContext)
    {
        Log logger = generatorContext.getLogger();

        String payloadDataType = payloadScanResult.getDataType();
        String packageName = ClassUtils.getPackageName(payloadDataType);
        String className = ClassUtils.getShortClassName(payloadDataType);

        String payloadJsonSchema = payloadScanResult.getSchema();

        JCodeModel codeModel = new JCodeModel();
        try
        {
            schemaMapper.generate(codeModel, className, packageName, payloadJsonSchema);

            logger.debug("Generate source code for payload class " + payloadDataType + " ...");
            codeModel.build(generatorContext.getOutputFolder());
        }
        catch (IOException e)
        {
            logger.error("Fail to generate source code for payload class" + payloadDataType, e);
        }
    }
}
