package com.atlassian.rest.retrofit.generator;

import org.apache.maven.plugin.logging.Log;

import java.io.File;

public class GeneratorContext
{
    private final File outputFolder;
    private final Log logger;

    public GeneratorContext(Log logger, File outputFolder)
    {
        this.logger = logger;
        this.outputFolder = outputFolder;
    }

    public Log getLogger()
    {
        return logger;
    }

    public File getOutputFolder()
    {
        return outputFolder;
    }
}
