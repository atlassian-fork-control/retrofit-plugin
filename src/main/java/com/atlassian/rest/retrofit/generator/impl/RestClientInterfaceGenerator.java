package com.atlassian.rest.retrofit.generator.impl;

import com.atlassian.rest.retrofit.generator.GeneratorContext;
import com.atlassian.rest.retrofit.scanresult.impl.ClassScanResult;
import com.atlassian.rest.retrofit.scanresult.impl.MethodScanResult;
import com.atlassian.rest.retrofit.scanresult.impl.ParamScanResult;
import com.atlassian.rest.retrofit.util.JavaFileUtil;
import com.google.common.collect.Maps;
import com.google.gson.JsonObject;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.maven.plugin.logging.Log;
import org.jboss.forge.roaster.Roaster;
import org.jboss.forge.roaster.model.source.JavaInterfaceSource;
import org.jboss.forge.roaster.model.source.MethodSource;
import org.jboss.forge.roaster.model.source.ParameterSource;

import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class RestClientInterfaceGenerator
{
    private final static String URL_PATH_SEPARATOR = "/";

    private final static Map<String, Class> retrofitMethodMapping = Maps.newHashMap();
    static
    {
        retrofitMethodMapping.put(GET.class.getCanonicalName(), retrofit.http.GET.class);
        retrofitMethodMapping.put(POST.class.getCanonicalName(), retrofit.http.POST.class);
        retrofitMethodMapping.put(PUT.class.getCanonicalName(), retrofit.http.PUT.class);
        retrofitMethodMapping.put(DELETE.class.getCanonicalName(), retrofit.http.DELETE.class);
    }

    private final static Map<String, Class> retrofitParamMapping = Maps.newHashMap();
    static
    {
        retrofitParamMapping.put(PathParam.class.getCanonicalName(), retrofit.http.Path.class);
        retrofitParamMapping.put(QueryParam.class.getCanonicalName(), retrofit.http.Query.class);
        retrofitParamMapping.put(FormParam.class.getCanonicalName(), retrofit.http.Field.class);
    }

    private static RestClientInterfaceGenerator restClientInterfaceGenerator = new RestClientInterfaceGenerator();

    private RestClientInterfaceGenerator() {}

    public static RestClientInterfaceGenerator get()
    {
        return restClientInterfaceGenerator;
    }

    public void generateRestClient(ClassScanResult classScanResult, GeneratorContext generatorContext)
    {
        JavaInterfaceSource restClientInterface = Roaster.create(JavaInterfaceSource.class);

        // 1. Package name and class name
        String packageName = classScanResult.getPackageName();
        String className = classScanResult.getClassName();
        restClientInterface.setPackage(packageName).setName(className);

        // 2. URL path at class level
        String basePath = StringUtils.removeEnd(classScanResult.getPath(), URL_PATH_SEPARATOR).trim();
        if (StringUtils.isBlank(basePath))
        {
            basePath = StringUtils.EMPTY;
        }
        if (!basePath.startsWith(URL_PATH_SEPARATOR))
        {
            basePath = URL_PATH_SEPARATOR + basePath;
        }

        // 3. REST API methods
        for (MethodScanResult methodScanResult : classScanResult.getMethods())
        {
            addMethod(restClientInterface, basePath, classScanResult.getProduces(), methodScanResult, generatorContext);
        }

        // 4. Write source code to file
        String fullQualifiedClassName = JavaFileUtil.getFullQualifiedClassName(packageName, className);
        File javaFile = JavaFileUtil.getJavaFile(generatorContext.getOutputFolder(), fullQualifiedClassName);
        Log logger = generatorContext.getLogger();
        try
        {
            writeRestClientInterfaceToFile(restClientInterface, fullQualifiedClassName, javaFile, logger);
        }
        catch (IOException e)
        {
            logger.error("Fail to generate source code for rest client interface " + fullQualifiedClassName, e);
        }
    }

    private void writeRestClientInterfaceToFile(JavaInterfaceSource restClientInterface,
                                                String fullQualifiedClassName,
                                                File javaFile,
                                                Log logger)
            throws IOException
    {
        if (!javaFile.exists() || !javaFile.isFile())
        {
            if (!logger.isDebugEnabled())
            {
                // This shit is to give maven log some beauties, because jsonschema2pojo
                // always prints out the java file path when it generates source code for payload classes
                System.out.println(JavaFileUtil.getRelativeJavaFilePath(fullQualifiedClassName));
            }

            logger.debug("Generate source code for rest client interface "
                    + fullQualifiedClassName + " ..."
                    + "\n" + JavaFileUtil.getRelativeJavaFilePath(fullQualifiedClassName)
                    + "\n" + restClientInterface);

            FileUtils.writeStringToFile(javaFile, restClientInterface.toString());
        }
        else
        {
            logger.debug("Skipped generating source code for rest client interface " + javaFile
                    + " because it exists at " + javaFile.getAbsolutePath() + " ...");
        }
    }

    private void addMethod(JavaInterfaceSource restClientInterface,
                           String basePath,
                           List<String> classProduceTypes,
                           MethodScanResult methodScanResult,
                           GeneratorContext generatorContext)
    {
        MethodSource<JavaInterfaceSource> method = restClientInterface.addMethod();

        // 1. URL path at method level
        String methodPath = StringUtils.removeStart(methodScanResult.getPath(), URL_PATH_SEPARATOR);
        if (StringUtils.isBlank(methodPath))
        {
            methodPath = StringUtils.EMPTY;
        }
        else
        {
            methodPath = URL_PATH_SEPARATOR + methodPath.trim();
        }

        // 2. Verb (GET/POST/PUT/DELETE) of the REST API method
        String path = basePath + methodPath;
        method.addAnnotation(retrofitMethodMapping.get(methodScanResult.getType())).setStringValue(path);

        // 3. Method name
        method.setName(methodScanResult.getName());

        // 4. The produces type, a.k.a return type of the REST API method
        List<String> methodProduceTypes = methodScanResult.getProduces();
        if (methodProduceTypes == null)
        {
            methodProduceTypes = classProduceTypes;
        }
        if (methodProduceTypes != null && methodProduceTypes.contains(MediaType.APPLICATION_JSON))
        {
            restClientInterface.addImport(JsonObject.class);
            method.setReturnType(JsonObject.class);
        }
        else
        {
            restClientInterface.addImport(retrofit.client.Response.class);
            method.setReturnType(retrofit.client.Response.class);
        }

        // 5. Method parameters
        for (ParamScanResult paramScanResult : methodScanResult.getParams())
        {
            addParam(method, paramScanResult, generatorContext);
        }
    }

    private void addParam(MethodSource<JavaInterfaceSource> method, ParamScanResult paramScanResult, GeneratorContext generatorContext)
    {
        ParameterSource<JavaInterfaceSource> param = method.addParameter(paramScanResult.getDataType(), paramScanResult.getName());

        if (paramScanResult.isPayload())
        {
            param.addAnnotation(retrofit.http.Body.class);
            PayloadClassGenerator.get().generate(paramScanResult.getPayload(), generatorContext);
        }
        else
        {
            param.addAnnotation(retrofitParamMapping.get(paramScanResult.getType())).setStringValue(paramScanResult.getName());
        }
    }
}
