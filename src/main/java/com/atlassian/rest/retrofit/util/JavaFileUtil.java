package com.atlassian.rest.retrofit.util;

import org.apache.commons.lang3.StringUtils;

import java.io.File;

public class JavaFileUtil
{
    private final static String DIR_PATH_SEPARATOR = "/";
    private final static String JAVA_CLASS_NAME_SEPARATOR = ".";
    private final static String JAVA_FILE_EXTENSION = ".java";

    public static File getJavaFile(File folder, String fullQualifiedClassName)
    {
        return new File(folder.getAbsolutePath() + DIR_PATH_SEPARATOR + getRelativeJavaFilePath(fullQualifiedClassName));
    }

    public static String getRelativeJavaFilePath(String fullQualifiedClassName)
    {
        return StringUtils.replace(fullQualifiedClassName, JAVA_CLASS_NAME_SEPARATOR, DIR_PATH_SEPARATOR) + JAVA_FILE_EXTENSION;
    }

    public static String getFullQualifiedClassName(String packageName, String className)
    {
        return packageName + JAVA_CLASS_NAME_SEPARATOR + className;
    }
}
