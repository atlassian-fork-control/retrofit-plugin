package com.atlassian.rest.retrofit.scanresult.impl;

import com.atlassian.rest.retrofit.scanresult.OptionalScanResult;

public class ParamScanResult extends OptionalScanResult
{
    private final static String PAYLOAD_TYPE = "payload";

    private String type;
    private String name;
    private String dataType;
    private PayloadScanResult payload;

    private ParamScanResult() {}

    public static ParamScanResult skip()
    {
        ParamScanResult skippedInstance = new ParamScanResult();
        skippedInstance.isSkipped = true;
        return skippedInstance;
    }

    public String getType()
    {
        return type;
    }

    public String getName()
    {
        return name;
    }

    public String getDataType()
    {
        return dataType;
    }

    public PayloadScanResult getPayload()
    {
        return payload;
    }

    public boolean isPayload()
    {
        return PAYLOAD_TYPE.equals(type);
    }

    public static class Builder
    {
        private String type;
        private String name;
        private String dataType;
        private PayloadScanResult payload;

        public Builder type(String type)
        {
            this.type = type;
            return this;
        }

        public Builder name(String name)
        {
            this.name = name;
            return this;
        }

        public Builder dataType(String dataType)
        {
            this.dataType = dataType;
            return this;
        }

        public Builder payload(PayloadScanResult payload)
        {
            type(PAYLOAD_TYPE);
            this.payload = payload;
            return this;
        }

        public ParamScanResult build()
        {
            ParamScanResult instance = new ParamScanResult();
            instance.dataType = this.dataType;
            instance.payload = this.payload;
            instance.name = this.name;
            instance.type = this.type;
            return instance;
        }
    }
}
