package com.atlassian.rest.retrofit.scanresult.impl;

public class PayloadScanResult
{
    private String dataType;
    private String schema;

    private PayloadScanResult() {}

    public String getDataType()
    {
        return dataType;
    }

    public String getSchema()
    {
        return schema;
    }

    public static class Builder
    {
        private String dataType;
        private String schema;

        public Builder dataType(String type)
        {
            this.dataType = type;
            return this;
        }

        public Builder schema(String schema)
        {
            this.schema = schema;
            return this;
        }

        public PayloadScanResult build()
        {
            PayloadScanResult instance = new PayloadScanResult();
            instance.schema = this.schema;
            instance.dataType = this.dataType;
            return instance;
        }
    }
}
