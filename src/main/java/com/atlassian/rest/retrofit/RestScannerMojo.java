package com.atlassian.rest.retrofit;

/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import com.google.common.collect.Sets;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;

import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Set;

@Mojo( name = "touch", defaultPhase = LifecyclePhase.PREPARE_PACKAGE )
public class RestScannerMojo extends AbstractMojo
{
    @Component
    private MavenProject project;

    @Parameter(property = "scanningPackage", required = true)
    private String scanningPackage;

    @Parameter(property = "generatedSourceCodeFolder", required = false, defaultValue = "retrofit-generated-source-code")
    private String generatedSourceCodeFolder;

    public void execute()
        throws MojoExecutionException
    {
        getLog().info("Scanning " + scanningPackage);

        RestScannerConfiguration configuration = new RestScannerConfiguration.Builder()
                .logger(getLog())
                .scanningPackage(scanningPackage)
                .urls(parseUrls())
                .dependencyArtifacts(getProject().getDependencyArtifacts())
                .generatedSourceCodeFolder(getProject().getBasedir() + "/" + generatedSourceCodeFolder)
                .build();

        RestScanner scanner = new RestScanner(configuration);
    }

    private URL parseOutputDirUrl() throws MojoExecutionException
    {
        try
        {
            File outputDirectoryFile = new File(resolveOutputDirectory() + '/');
            return outputDirectoryFile.toURI().toURL();
        }
        catch (MalformedURLException e)
        {
            throw new MojoExecutionException(e.getMessage(), e);
        }
    }

    private Set<URL> parseUrls() throws MojoExecutionException
    {
        final Set<URL> urls = Sets.newHashSet();
        urls.add(parseOutputDirUrl());

        return urls;
    }

    private String resolveOutputDirectory()
    {
        return getProject().getBuild().getOutputDirectory();
    }

    public MavenProject getProject()
    {
        return project;
    }
}
